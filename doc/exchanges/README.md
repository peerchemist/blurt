# Blurt Exchange Documentation

Welcome to the Blurt documentation for exchanges.  Here, you can find a fully-auto setup at [exchanges.md](exchanges.md) and a manual setup that uses Docker at [exchange_manual.md](exchange_manual.md).

If you've got any questions for us, please don't hesitate to contact exchanges@blurt.world 



 
